const gulp = require('gulp');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const fileinclude = require('gulp-file-include');

const processors = [
    require('postcss-import'),
    require('postcss-custom-properties'),
    require('postcss-custom-media'),
    require('postcss-nesting'),
    require('postcss-nested'),
    require('postcss-inline-svg'),
    require('postcss-pxtorem'),
    require('cssnano')({
        autoprefixer: false,
        reduceIdents: false
    })
];

gulp.task('php-pcss', (done)=> {
    return gulp
        .src('./src/css/*.pcss')
        .pipe(postcss(processors))
        .pipe(rename({
            extname: ".css"
        }))
        .pipe(gulp.dest('./dist/css/'))
        .on('finish', ()=> {
            done();
        });
});

gulp.task('php-js', ()=> {
    return gulp
        .src('./src/js/*.js')
        .pipe(gulp.dest('./dist/js/'))
});

gulp.task('php-fileinclude', (done) => {
    return gulp
        .src(['src/*.html'])
        .pipe(fileinclude({
        prefix: '@@',
        }))
        .pipe(rename({ extname: '.php' }))
        .pipe(gulp.dest('./dist/'))
        .on('finish', ()=> {
            done();
        });
});

gulp.task('php-img', ()=> {
    return gulp
        .src('./src/img/**/**')
        .pipe(gulp.dest('./dist/img/'))
        
});

gulp.task('php-img-ico', ()=> {
    return gulp
        .src('./src/*.ico')
        .pipe(gulp.dest('./dist/'))
        
});

// gulp.task('php-browser-sync', ()=> {
//     browserSync.init({
//         // files: ['./dist/index.html'],
//         server: {
//             baseDir: "./dist/",
//             directory: true
//         },
        
//     });
// });
// gulp.task('php-server-reload', (done)=> {
//     browserSync.reload();
//     done();
// });

gulp.task('watch:php-pcss', ()=> {
    gulp.watch('./src/css/*', gulp.series('php-pcss'));
});

gulp.task('watch:php-html', ()=> {
    gulp.watch('./src/**/*.html', gulp.series('php-fileinclude'));
});

gulp.task('watch:php-js', ()=> {
    gulp.watch('./src/js/*', gulp.series('php-js'));
});

gulp.task('watch:php-img', ()=> {
    gulp.watch('./src/img/**/**', gulp.series('php-img'));
});

gulp.task('watch', gulp.parallel('watch:php-pcss', 'watch:php-html', 'watch:php-js','watch:php-img'));
gulp.task('build', gulp.parallel('php-pcss', 'php-js','php-fileinclude', 'php-img','php-img-ico'));

