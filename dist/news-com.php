<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку новостей</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/news/1.svg" alt="img6">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__news-date">01.02.2019</div>
                    <div class="blogs-rep__news-title">Работа в команде: правила выживания</div>
                    <div class="blogs-rep__news-disc">Итак, в предыдущей статье  <a href="#">"Эффективное управление: рукоприкладство или психология?"</a> мы познакомились с основными типами личности. Здесь же мы посмотрим, как полезно знать, кто твои соседи по офису и какой стороной повернуться к коллеге, чтобы пазлы наконец-то сложились.</div>
                </div>
                <div class="news-content">
                    <div class="row-com">
                        <div class="title">Любой тип очень удобно вычислять по тому, как он ведет себя в конфликте:</div>
                    </div>
                    <div class="row-com__block">
                        <div class="col">
                            <div class="img img-ell"></div>
                        </div>
                        <div class="col col-disc">
                            <div class="texttitle texttitle-pink">Конфликтующий эпилептоид</div>
                            <p>просто создан для того, чтоб жестить и возмущаться. Помните, что любой конфликт вскрывает мутные непонятки. И хотя эпилептоид периодами тих, как большая река, это до начала порогов и водопадов.</p>
                            <div class="textdisc">Что делать?</div>
                            <p>
                                Перестать воспринимать эти всплески на свой счет. А еще помните: эпилептоиды максимально логичны, поэтому с ними хорошо работают фразы: “я тебя понял”, “в чем ты видишь суть проблемы?” и “мне тоже это не нравится, я готов решать”.
                            </p>
                        </div>
                    </div>
                    <div class="row-com__block row-com__block-revers">
                        <div class="col">
                            <div class="img img-shizoid"></div>
                        </div>
                        <div class="col col-disc">
                            <div class="texttitle texttitle-green">Конфликтующий шизоид</div>
                            <p>ввиду своей замкнутости на открытый конфликт не идет. И если вы с ним не в контакте, то можете никак не заметить, что что-то пошло не так. Поэтому о напряжении судить можно по косвенным моментам. К примеру, в работе у вас с коллегой-шизоидом одно направление, но разные задачи. И если до конфликта он благосклонно брал на себя чуть больше положенных ему функций, то теперь, справедливости ради, он выровняет чаши весов. Это даже пассивной агрессией не назовешь, просто ему сложно выразить свою позицию в открытую.</p>
                            <div class="textdisc">Что делать?</div>
                            <p>
                                Поговорить. Это вообще единственный тип, с кем можно действительно это сделать. Не супердоверительно, но все же. Просто, четко, без обвинений изложите то, как вы видите ситуацию. Если он не захочет говорить, сообщите, что открыты к диалогу и готовы продолжить, когда ему это будет удобно.
                            </p>
                        </div>
                    </div>
                    <div class="row-com__block">
                        <div class="col">
                            <div class="img img-ist"></div>
                        </div>
                        <div class="col col-disc">
                            <div class="texttitle texttitle-pinkin">Конфликтующий истероид</div>
                            <p>театр одного актера, поэтому слезы-сопли-угрозы расправы с линейкой в руках - это всего лишь проявления его излишней эмоциональности. Но если у истероида есть эпилептоидные черты (чистых типов практически не бывает), тогда он может быть агрессивным. Так что в конфликте лучше держаться от него подальше.</p>
                            <div class="textdisc">Что делать?</div>
                            <p>
                                Подождать, пока закончится представление. Истероид, как капризный ребенок, устанет и захочет спать. И вот тогда можно сказать, что вы очень понимаете, как ему тяжело. Здесь важно особо не впечатляться, и тогда разговор может получиться вполне сносный.
                            </p>
                        </div>
                    </div>
                    <div class="row-com__disc">
                        <div class="col">
                            <div class="title">Что получается?</div>
                            <p class="disc">С кем бы мы не имели дело, цель одна - выйти на контакт. Просто способы ее достижения разные.</p>
                        </div>
                        <div class="col">
                            <div class="title">Но что делать, если вы обнаружили в одном из этих типов себя?</div>
                            <p>
                                Не ждите, пока коллеги догадаются о вашем настроении. Вы не младенец, они вам не родители. Поэтому если кризисы возникают с известной периодичностью, то сообщите всем заранее, что в 26-е лунные сутки у вас наступает повышенная лохматость и хвост отваливается. А на случай апериодичного напряжения используйте реакцию в моменте: о своих чувствах говорите здесь и сейчас.
                            </p>
                        </div>
                    </div>
                    <div class="row-com__after">
                        <div class="col col-color">И помните:&#160;&#160;</div>
                        <div class="col col-disc">за окружающими всегда остается право не понять вас. Как и за вами - постараться быть понятым.</div>
                    </div>
      
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>    
