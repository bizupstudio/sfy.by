
// var $ = require("jquery");




$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    items: 1,
    margin: 10,
    nav: true,
    loop: true
  });
});


$( document ).ready(function() {
  $(".company__form").submit(
    function(e){
       e.preventDefault();

      // $('.company__form-btn').attr(disabled);
      sendAjaxForm('.company__form', 'send2.php');
      // return false; 
      
        function sendAjaxForm(ajax_form, url) {
          $.ajax({
              url:     url, //url страницы (send.php)
              type:     "POST", //метод отправки
              dataType: "html",
              data: $(ajax_form).serialize(),  // Сеарилизуем объект
              beforeSend: function(data) { // событие до отправки
              
        		            $('.company__form-btn').attr('disabled', 'disabled');
        		            
        		          },
              success: function(response) { //Данные отправлены успешно
                result = $.parseJSON(response);
                //console.log(result);
                $(".company__form").trigger("reset");
                $('.modal__name').html(result.name);
                $('.overlay').addClass('overlay-active');
                
                function fun() {
                  $('.overlay').removeClass('overlay-active');
                }
        
                setTimeout(fun,3000);
                $('.company__form-btn').removeAttr('disabled');
        
                //console.log('Имя: '+result.name+'<br>Телефон: '+result.phone);
            },
            error: function(response) { // Данные не отправлены
              console.log('Ошибка. Данные не отправлены.');
            }
         });
        }
      
    })
    

});

//btn prev 
$(function(){
  $('.wrapper-btn-prev').click(function(){
    window.location.href = "news.php"
  })
})


//btn in page keys
$(function(){
  $('.wrapper-btn-prev-keys').click(function(){
    window.location.href = "keys.php"
  })
})

//btn index
$(function(){
  $('.wrapper-btn-prevIndex').click(function(){
    window.location.href = "index.php";
  })
})

$(function(){

  //console.log('document.referrer= '+ document)  
  let locat = 'https://sfy.by/';


  if (document.referrer == `${locat}news.php`) {
    $('.firstblock').fadeOut(1000);
    $('.wrapper').fadeIn(1000);
    
    $("#fullpage").fullpage({
      navigation: true,
      navigationPosition: 'left',
    })
    $.fn.fullpage.moveTo(6);
  }
  if (document.referrer == `${locat}keys.php`) {
    $('.firstblock').fadeOut(1000);
    $('.wrapper').fadeIn(1000);
    
    $("#fullpage").fullpage({
      navigation: true,
      navigationPosition: 'left',
    })
    $.fn.fullpage.moveTo(5);
  }


})


//filter 
$(function(){
  $('.sort-param').click(function(){
    $('.sort-alert').toggleClass('sort-alert-active');
  })

  $('.sort-name').click(function(){
    let param = $(this).html();
    $('.sort-param').html(param);
    $('.sort-alert').toggleClass('sort-alert-active');
  })

  $(document).click(function(e){
    


    if ( $('.sort-alert').css('display') == 'block'){
      let btn = $('.sort-param');
      let modal = $('.sort-alert');
			if (!modal.is(e.target) && modal.has(e.target).length === 0 && !btn.is(e.target)) {
					$('.sort-alert').removeClass('sort-alert-active');
      } 
      
      //console.log(modal.is(e.target));
		};

  })
})



$(function(){

  $('.arrow-disc').click(function(){
    console.log('click')
    $('.arrow').click();
    setTimeout(()=>{
      $('.wrapper-btn').click();
    },500)
    

  })

  function myFunction(x) {
    if (x.matches) { 
     
    } else {
      $(window).scroll(function(){
        if ($(this).scrollTop() > 500) {
          $('.arrow-disc').fadeIn();
        } else {
          $('.arrow-disc').fadeOut();
        }
      })
    }
  }

  var x = window.matchMedia("(max-width: 1200px)")
  myFunction(x)


  $(window).scroll(function(){
 
    if ($(this).scrollTop() > 500) {
      $('.arrow').fadeIn();
    } else {
      $('.arrow').fadeOut();
  
    }
  })
  $('.arrow').click(function(){
    $('html').animate({
      scrollTop: 0
    })
  })

})

$(function(){
  $('.firstblock-btn').on('click', function(){
    $('.firstblock').fadeOut(1000);
    $('.wrapper').fadeIn(1000);
    
    $("#fullpage").fullpage({
      navigation: true,
      navigationPosition: 'left',
      afterLoad:function(link,index) {

        switch (index) {
          case 1: 
            $('.company__title-spanCounter').html("01");
            break;
        
          case 2: 
            $('.company__title-spanCounter').html("02");
            break;

          case 3: 
            $('.company__title-spanCounter').html("03");
            break; 

          case 4: 
            $('.company__title-spanCounter').html("04");
            break;   

          case 5: 
            $('.company__title-spanCounter').html("05");
            break; 
        }
      }
    })
  })

  $('.firstblock__content__logo').on('click', function(){
    $('.firstblock').fadeOut(1000);
    $('.wrapper').fadeIn(1000);
    
    $("#fullpage").fullpage({
      navigation: true,
      navigationPosition: 'left',
      afterLoad:function(link,index) {

        switch (index) {
          case 1: 
            $('.company__title-spanCounter').html("01");
            break;
        
          case 2: 
            $('.company__title-spanCounter').html("02");
            break;

          case 3: 
            $('.company__title-spanCounter').html("03");
            break; 

          case 4: 
            $('.company__title-spanCounter').html("04");
            break;   

          case 5: 
            $('.company__title-spanCounter').html("05");
            break; 
        }
      }
    })
  })


  $('.firstblock-btn-mobile').on('click', function(){
    $('.firstblock').fadeOut(1000);
    $('.wrapper').fadeIn(1000);
    
    $("#fullpage").fullpage({
      navigation: true,
      navigationPosition: 'left',
      afterLoad:function(link,index) {
        switch (index) {
          case 1: 
            $('.company__title-spanCounter').html("01");
            break;
        
          case 2: 
            $('.company__title-spanCounter').html("02");
            break;

          case 3: 
            $('.company__title-spanCounter').html("03");
            break; 

          case 4: 
            $('.company__title-spanCounter').html("04");
            break;   

          case 5: 
            $('.company__title-spanCounter').html("05");
            break; 
        }
      }
   });
  })
  

  $('.wrapper-btn-index').on('click', function(){
    
    $('.wrapper').css('display', 'none')
    $('.firstblock').fadeIn();
    $.fn.fullpage.destroy('all');

    $('.firstblock-btn').css('top','50%');
  })
})

$(function(){
  var colors = new Array(
    [62,35,255],
    [60,255,60],
    [255,35,98],
    [45,175,230],
    [255,0,255],
    [255,128,0]);
  
  var step = 0;
  //color table indices for: 
  // current color left
  // next color left
  // current color right
  // next color right
  var colorIndices = [0,1,2,3];
  
  //transition speed
  var gradientSpeed = 0.002;
  
  function updateGradient()
  {
    
    if ( $===undefined ) return;
    
  var c0_0 = colors[colorIndices[0]];
  var c0_1 = colors[colorIndices[1]];
  var c1_0 = colors[colorIndices[2]];
  var c1_1 = colors[colorIndices[3]];
  
  var istep = 1 - step;
  var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
  var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
  var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
  var color1 = "rgb("+r1+","+g1+","+b1+")";
  
  var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
  var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
  var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
  var color2 = "rgb("+r2+","+g2+","+b2+")";
  
   $('#gradient').css({
     background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
      background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});
    
    step += gradientSpeed;
    if ( step >= 1 )
    {
      step %= 1;
      colorIndices[0] = colorIndices[1];
      colorIndices[2] = colorIndices[3];
      
      //pick two new target color indices
      //do not pick the same as the current one
      colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
      colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
      
    }
  }
  
  setInterval(updateGradient,5);
})
