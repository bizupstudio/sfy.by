<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prevIndex">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>
    <div class="arrow"></div>
    <div class="news-container">
        <div class="sort">
            <span class="sort-name">Сортировать по:</span><span class="sort-param">сначала новые</span>
            <div class="sort-alert">
                <ul>
                    <li>
                        <span class="sort-name">cначала новые</span>
                    </li>
                    <li>
                        <span class="sort-name">cначала старые</span>
                    </li>
                    <li>
                        <span class="sort-name">количеству просмотров</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="blogs">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-items">
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/1.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Работа в команде: правила выживания</div>
                        <div class="caption-text">
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: 
                            рукоприкладство или психология?" и что к
                            <div class="caption-text-shadow"></div>
                        </div>
                        <a href="news-com.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/6.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Диверсанты бизнеса
                        </div>
                        <div class="caption-text">
                            <div class="caption-text-shadow"></div>
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: рукоприкладство или психология?" и что к</div>
                        <a href="news-divers.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/2.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Эффективное управление: рукоприкладство или психология?
                        </div>
                        <div class="caption-text">
                            <div class="caption-text-shadow"></div>
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: рукоприкладство или психология?" и что к</div>
                        <a href="news-eff.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/3.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Продажный дизайн против продающего: битва за клиента
                        </div>
                        <div class="caption-text">
                            <div class="caption-text-shadow"></div>
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: рукоприкладство или психология?" и что к</div>
                        <a href="news-design.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/4.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Не лезьте в черный ящик</div>
                        <div class="caption-text">
                            <div class="caption-text-shadow"></div>
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: рукоприкладство или психология?" и что к</div>
                        <a href="news-box.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/5.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">За чем идти в "долину смерти" 
                        </div>
                        <div class="caption-text">
                            <div class="caption-text-shadow"></div>
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: рукоприкладство или психология?" и что к</div>
                        <a href="news-dead.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="blogs">
            <div class="blogs-title blogs-title-report">Репортаж</div>
            <div class="blogs-items">
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/1.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Эффективное управление: рукоприкладство или психология?</div>
                        <div class="caption-text">
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: 
                            рукоприкладство или психология?" и что к
                            <div class="caption-text-shadow"></div>
                        </div>
                        <a href="rep1.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="blogs">
            <div class="blogs-title blogs-title-razbor">Разбор</div>
            <div class="blogs-items">
                <div class="blogs-item">
                    <div class="img">
                        <img src="img/news/1.svg" alt="">
                        <div class="maska-face"></div>
                        <div class="maska-back"></div>
                    </div>
                    <div class="caption">
                        <div class="caption-date">12.11.2019</div>
                        <div class="caption-title">Эффективное управление: рукоприкладство или психология?</div>
                        <div class="caption-text">
                            В предыдущих статьях стало ясно, что все мы разные "Эффективное управление: 
                            рукоприкладство или психология?" и что к
                            <div class="caption-text-shadow"></div>
                        </div>
                        <a href="razb1.php" class="caption-link">Читать далее</a>
                    </div>
                </div>
            </div>
        </div> --> 

    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>   
