<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev-keys">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку кейсов</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="keys-title keys-title-blue">Коммерческий проект</div>
            <h1 class="keys-nametitle keys-nametitle-slav">Бренд г. Славгород и Славгородского района</h1>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/keys/slav/slav-golo.svg" alt="img1">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__date-title blogs-rep__caption-blue">Период сотрудничества:</div>
                    <div class="blogs-rep__date-text blogs-rep__caption-disc-slav">сентябрь 2017</div>
                    <div class="blogs-rep__location-title blogs-rep__caption-blue">Задача:</div>
                    <div class="blogs-rep__location-text blogs-rep__caption-disc-slav">Проработка составляющих бренда (концепция, визуализация, рекомендации по продвижению</div>
                    <div class="blogs-rep__konzept-title blogs-rep__caption-blue">Детали:</div>
                    <div class="blogs-rep__konzept-text blogs-rep__caption-disc-slav">В рамках реализации местной инициативы «Туристический Славгород» проекта «Содействие развитию на местном уровне в Республике Беларусь», финансируемого Европейским Союзом и реализуемого Программой развития ООН была начата разработка бренда г. Славгорода и Славгородского района.</div>
                </div>
            </div>
            <div class="keys__content-slav">
                <div class="work-title">КАК МЫ РАБОТАЛИ?</div>
                <div class="work-disc__title">Славгородский район расположен в южной части Могилевской области на расстоянии 68 км от областного центра в бассейне рек Проня и Сож, площадь района составляет 1,3 тыс. кв. км, население составляет 7812 человек (1 января 2016 года).</div>
                <div class="work-row">
                    <div class="work-left">
                        В процессе работы было собрано и проведено 2 фокус-группы, были выявлены особенности и возможности Славгородского района, а также критерии полезности территории района для создания уникального бренда, в результате чего были определены 5 концепций предполагаемого бренда и, соотвественно, разработаны 5 пар "логотип + слоган" на основании выбранных концепций. Завершением работы стала итоговая презентация бренда г. Славгорода и Славгородского района, организованная при поддержке местного фонда развития сельских территорий «Возрождение-Агро».
                    </div>
                    <div class="work-right">
                        <div class="work-right-bg"></div>

                    </div>
                </div>
                <div class="vars-logo">
                    <div class="vars-logo__title">Варианты логотипа </div>
                    <div class="vars-logo-row"> 
                        <div class="img-1"></div>
                        <div class="img-2"></div>
                    </div>
                    <div class="vars-logo-row vars-logo-row-fend"> 
                        <div class="img-3"></div>
                        <div class="img-4"></div>
                        <div class="img-5"></div>
                    </div>
                    <div class="vars-logo__title">Итоговый вариант</div>
                    <div class="itog-row">
                        <div class="itog-row__img"></div>
                        <div class="itog-row__disc">
                            <div class="title">Цветовая палитра (CMYK)</div>
                            <ul>
                                <li>
                                    <span class="BDAB50"></span> – BDAB50
                                </li>
                                <li>
                                    <span class="D4CA4D"></span> – DACC53
                                </li>
                                <li>
                                    <span class="FFD340"></span> – EED15F
                                </li>
                                <li>
                                    <span class="FAEE16"></span> – F0E355
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="vars-logo__title vars-logo__title-min">Слоган</div>
                    <div class="vars-logo__disc vars-logo__disc-mrb80">Славгород – город, который я люблю!</div>
                    <div class="vars-logo__title vars-logo__title-min">Обоснование дизайна</div>
                    <div class="vars-logo__disc vars-logo__disc-mrb40 ">В основу логотипа легло несколько элементов:</div>
                    <div class="dizain-row">
                        <div class="col col-1">
                            скомпилированное название города Славгород и фразы “славный город” – такой способ отображения является тендерцией в брендировании городов, хорошо запоминается потребителями, отлично подходит для создания фотозон и рекламной продукции.
                        </div>
                        <div class="col col-2">
                            логотип с очертаниями города – так как в основе концепции лежит мысль “я люблю Славгород за то, какой он есть”. Логотип выполнен в виде очертаний самого города (часть элементов сделала на основе видоизмененных очертаний настоящих строений Славгорода и Славгородского района), что для потребителя будет являться отражением “души” города.
                        </div>
                        <div class="col col-3">
                            Слово “славный”, использованное в логотипе, имеет несколько значений. При создании  логотипа было принято решение основным выбрать значение “известный/знаменитый” (т.к. брендирование территории Славгородского района направленно именно на получение известности), что отразилось на выборе цветов.
                        </div>
                    </div>
                    <div class="vars-logo__title">Детали дизайна</div>
                    <div class="diz-row">
                        <div class="col">В логотипе использованы различные оттенки золотого для создания прочной ассоциации у потребителя “Славгород – знаменитый ” (по аналогии со знаменитостями, которые часто представляются нам в чем-то золотом или сверкающем). При этом текст на логотипе может быть изображен в любом другом подходящем цвете (это актуально и для очертаний города, но не стоит менять цветомую гамму без особо важной причины – нарушится ассоциация).</div>
                        <div class="col">Наиболее подходящие цвета для текста на логотипе или для других элементов оформления при создании рекламной продукции: черный, белый, оттенки желтого. При внедрении бренда и единого фирменного стиля можно использовать как изображение всего логотипа, так и его отдельного элемента – “город”.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>   
