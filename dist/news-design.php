<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку новостей</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/news/3.svg" alt="img6">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__news-date">12.11.2019</div>
                    <div class="blogs-rep__news-title">Продажный дизайн против продающего: битва за клиента
                    </div>
                    <div class="blogs-rep__news-disc">Каждый день мы плаваем по Интернет-просторам. И хочется, чтобы ветер попутный, чтобы корабль удобный, чтобы глаз радовала ровная морская гладь. Это типа метафора про сайты: хочу быстро, четко и без загонов найти то, что мне нужно. Поэтому советы, размещенные ниже, для тех, кому приходится организовывать этот «круиз». А еще чтобы дизайнерам и строителям сайтов внезапно стало ясно, как продать, а не продаться.</div>
                </div>
                <div class="news-content">
                    <div class="design">
                        <div class="design-item">
                            <div class="design__title design__title-one">Минимализм</div>
                            <div class="design__disc">
                                <p>Время нынче такое, что сайт должен быть компактным, четким, кратким. Это касается и текста, и графики. Они не должны перегружать пользователя громоздкими названиями и блоками.</p>
                                <p><span>Внимание:</span> Эффект «Санта-Барбары». Не выносите на главную страницу сразу всю информацию, пусть там появятся краткие текстовые блоки с ключевыми словами поиска. Покажите коленку, а не сразу целую ногу.</p>
                                <p>Мы с вами делаем не презентацию в PowerPoint для семинара в универе, так что текст поверх фото размещать не нужно. Придется ведь добавлять тень, свечение, градиент. Время олдскула прошло еще лет десять назад, поэтому чтобы быть в тренде, купите себе белые джинсы и сделайте картинку намеренно простой и «плоской».</p>   
                            </div>
                        </div>
                        <div class="design-item">
                            <div class="design__title design__title-two">Доступность информации</div>
                            <div class="design__disc">
                                <p>Все просто: замените названия текстовых блоков иконками. Например, вместо слова «телефон» разместите изображением телефона.</p>
                                <p><span>Внимание:</span> текст нужен только там, где без него не обойтись. Скажем «нет!» деепричастным оборотам и всему, что можно взять в скобки. И помним: структурированность, четкость, последовательность.</p>
                                <p>А еще будет здорово, если вы оставите на главной странице «заманушку» в виде трех-четырех блоков с записями из разряда «лучшее», «последнее». Аперитив, чтоб разогреть публику.</p>   
                            </div>
                        </div>
                        <div class="design-item">
                            <div class="design__title design__title-three">Единый стиль</div>
                            <div class="design__disc">
                                <p>Во-первых, выберите основной и контрастный ему цвета для заголовков, логотипа. Во-вторых, для фона подойдут черный или белый. А если хочется более мягкого ощущения от цветовой схемы сайта, выбирайте темно- или светло-серый цвета.</p>
                                <p> Ширина блоков, рамок, отступов между ними, да и в целом их пропорции впишите в общий стиль и сохраняйте его на всех страницах. Вы же хотите создать целостное представление о сайте, вы же хотите выглядеть продавабельно.</p>
                                <p><span>Внимание:</span> в первую очередь займитесь структурой сайта. Дизайн сообразите после. То, что у вас основной цвет не сочетается с контрастным, еще куда ни шло, но если в навигации черт ногу сломит, то вам предпочтут более структурированных конкурентов.</p> 
                            </div>
                        </div>
                        <div class="design-item">
                            <div class="design__title design__title-fhour">Ориентация на пользователя</div>
                            <div class="design__disc">
                                
                                
                                <p><span>Здесь одно правило:</span> исключите любые личные предпочтения. Ваша задача таким образом соединить картинку, текст и структуру страницы, чтобы пользователю очень захотелось не просто добавить ее в закладки, а еще и дать вам денежку за услугу.</p> 
                                <p>Вот вы пойдете сейчас воплощать эти советы в жизнь и заметите, что схема работает, а потом отправите нам астральное «спасибо». Ну или перейдете по ссылке дальше, чтобы увидеть коленку ;)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>    
