<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку новостей</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/news/6.svg" alt="img6">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__news-date">01.02.2019</div>
                    <div class="blogs-rep__news-title">Диверсанты бизнеса:</div>
                    <div class="blogs-rep__news-disc">В предыдущих статьях стало ясно, что все мы разные <a href="#">"Эффективное управление: рукоприкладство или психология?"</a> и что к каждому из нас можно найти подход <a href="#">"Работа в команде: правила выживания"</a>.</div>
                </div>
                <div class="news-content">
                    <div class="row-devers">
                        <div class="col">
                            <p>Есть настоящие диверсанты, кашу с которыми лучше не варить. Это так называемые представители неустойчивого типа.</p>
                             <h2>Посмотрим, что у них там внутри:</h2>
                                <ul>
                                    <li>большие вопросы с волей, ответственностью;</li>
                                    <li>как правило, они ленивы;</li>
                                    <li>по-детски наивны, часто открыты, общительны, услужливы. На этапе знакомства это может сбить вас с толку;</li>
                                    <li>непреодолимая тяга к удовольствиям;</li>
                                    <li>непоследовательность;</li>
                                    <li>размытое представление о личных границах, мире и ожиданиях</li>
                                </ul>
                                <p>
                                    Так что если на собеседовании или во время испытательного срока вы заметили какие-то из этих черт, долго не думайте: избавьте себя от запланированного треша. В конце концов, вряд ли перед вами доктор Хаус.
                                </p>
                        </div>
                        <div class="col">
                            <p>Что делать, если такой сотрудник уже часть вашей команды? Можно сразу уволить. Или поставить его в условия максимальной четкости, без поблажек, на равных со всей командой.
                            </p>
                            <h2>Это значит, что на контроле вы держите:</h2>
                            <ul>
                                <li>норму выработки;</li>
                                <li>показатели;</li>
                                <li>режим труда и график работы</li>
                            </ul>
                            <p>
                                А еще такой сотрудник должен знать, за что вы ему сделаете "атата". Что в итоге? Неустойчивым ребятам сложно выдерживать дисциплину. Поэтому они найдут повод, уйдут сами.

                            </p>
                        </div>
                    </div>

                    <div class="news-wrapper-img">                    
                    </div>
                    <div class='news-diver__title'>
                        Ну а если вас уже связывает история с неустойчивым клиентом, от которой просто так не отделаться, то важно:
                        <ul>
                            <li>сразу требовать четкое ТЗ;</li>
                            <li>сообщить клиенту, что 80% от общей суммы в качестве предоплаты - условия для старта проекта; </li>
                            <li>И все. Вам больше делать ничего не надо. Отвалятся сами.</li>
                        </ul>
                    </div>
                    <div class="news-diver__ps">
                        <div class="row">
                            <div class="col">***</div>
                            <div class="col col-disc">
                                А что если эту статью читает какой-нибудь неустойчивый сотрудник или клиент?<br> Не читает. Потому что он сейчас едет в электричке в Одессу знакомиться с художницей из Chatroulette.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>  
