<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev-keys">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку кейсов</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="keys-title keys-title-pink">Некоммерческий проект</div>
            <h1 class="keys-nametitle">Библионочь 2016</h1>
            <h3 class="keys-nametitledisc">Международная акция, Научная библиотека БНТУ</h3>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/keys/biblionoch/bnoch.png" alt="img1" class="keys-radius">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__date-title blogs-rep__caption-pink">Период сотрудничества:</div>
                    <div class="blogs-rep__date-text blogs-rep__caption-disc">апрель 2016</div>
                    <div class="blogs-rep__location-title blogs-rep__caption-pink">Задача:</div>
                    <div class="blogs-rep__location-text blogs-rep__caption-disc">Разработка части программы мероприятия: организация мастер-классов и лекций на тему музыки и огня</div>
                    <div class="blogs-rep__konzept-title blogs-rep__caption-pink">Детали:</div>
                    <div class="blogs-rep__konzept-text blogs-rep__caption-disc">Выбор тем мастер-классов, поиск лекторов, тайминг, контроль реализации разработанной программы.</div>
                </div>
            </div>
            <div class="keys__content">
                <div class="keys__sliderBlock">
                    <div class="keys__sliderBlock-text">
                        <div class="keys__sliderBlock-text-title">Подготовка программы</div> 
                        <div class="keys__sliderBlock-text-disc">К организации мероприятия мы подключились за две недели до события, но на самом деле у нас было всего 7 дней до запуска первых публикаций с подробностями программы Библионочи. За это время мы подготовили программу тематических лекций и мастер-классов на тему музыки и огня, согласовали ее с участниками и предоставили информацию для новостных постов и пресс-релизов.</div> 
                        <div class="keys__sliderBlock-text-title">Площадка</div> 
                        <div class="keys__sliderBlock-text-disc">Были задействованы помещения различного формата на трех этажах Библиотеки БНТУ (фойе, конференц-зал, полуоткрытые помещения в читальном зале библиотеки).
                            Всего было организовано 3 лекции, 5 мастер-классов и театрализзованное огненное представление на весь вечер от <span> Театра огня и фарса "Balagan Band"</span>, который встречал гостей в фойе и показывал, как обращаться с таким инструментом, как огненный пой.</div> 
                    </div>
                    <div class="keys__sliderBlock-img">
                        <div class="owl-carousel owl-carousel-bibnoch">
                            <div> <img src="img/keys/biblionoch/3.png" alt="image" /> </div>
                            <!-- <div> <img src="img/keys/biblionoch/5.png" alt="image" /> </div> -->
                          </div>
                    </div>
                </div>
                <div class="keys__sectionMoney">
                    <div class="keys__sectionMoney-title">Бюджет и проведение</div>
                    <div class="keys__sectionMoney-disc">На гонорары участникам средства не выделялись. Все было реализовано с помощью метода win-win.</div>
                    <div class="keys__sectionMoney-midtitle">Во время самого мероприятия мы обеспечивали реализацию разработанного сценария:</div>
                    <div class="keys__sectionMoney__blocks">
                        <div class="item">
                            <div class="item__icon item__icon-calendar"></div>
                            <div class="item__text">контроль тайминга</div>
                        </div>
                        <div class="item">
                            <div class="item__icon item__icon-woman"></div>
                            <div class="item__text">встреча и сопровождение участников</div>
                        </div>
                        <div class="item">
                            <div class="item__icon item__icon-music"></div>
                            <div class="item__text">обеспечение их необходимой аппаратурой</div>
                        </div>
                        <div class="item">
                            <div class="item__icon item__icon-teamwork"></div>
                            <div class="item__text">корректировка потоков посетителей</div>
                        </div>
                        <div class="item">
                            <div class="item__icon item__icon-microphone"></div>
                            <div class="item__text">оказание технической помощи во время проведения музыкальной части программы</div>
                        </div>
                    </div>
                </div> 
                <div class="keys__result">
                    <div class="keys__result__title">Результат</div>
                </div>
                <div class="keys__smi">
                    <div class="item">
                        <div>Более</div>
                        <div class="number">500</div>
                        <div>посетителей<br>
                            за вечер</div>
                    </div>
                    <div class="item">
                        <div>Публикации в СМИ:</div>
                        <ul>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Интерфакс</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Another.by</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Blizko.by</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Белорусская военная газета "ВСР"</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Кактутжить</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Национальная библиотека Беларуси</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Афиша TUT.BY</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">СТВ</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Международное информационное агенство "Sputnik Беларусь"</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Журнал CityDog.by</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Naviny.by</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Агенство "Минск-Новости"</a>
                            </li>
                            <li>
                                <a href="https://vk.com/biblionoch_minsk_2016" target="_blank">Белорусское телеграфное аенство "Белта"</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>  
