<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prevIndex">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>
    <div class="arrow"></div>
    <div class="news-container keys">
        <div class="keys-running">
            <div class="keys-running-item">
                <div class="keys-running-item-img keys-running-item-img-pink"></div>
                <span> – некоммерческие проекты</span>
            </div>
            <div class="keys-running-item">
                <div class="keys-running-item-img keys-running-item-img-blue"></div>
                <span> – коммерческие проекты</span>
            </div>
            <div class="keys-running-item">
                <div class="keys-running-item-img keys-running-item-img-yellow"></div>
                <span> – менторские проекты</span>
            </div>
        </div>
        <div class="keys-blocks">
            <a href="keys-slavgorod.php" class="keys-blocks__item item item-slavgorod">
                <div class="item-color item-color-blue"></div>
                <div class="item-bg-left"></div>
                <div class="item-title item-title-slavgorod">
                    Бренд г. Славгород и
                    Славгородского района
                </div>
            </a>

            <a href="keys-krokapp.php" class="keys-blocks__item item item-krokapp">
                <div class="item-color item-color-yellow"></div>
                <div class="item-bg-left"></div>
                <div class="item-title item-title-slavgorod">
                    «KrokApp»

                    <span>Персанальны аўдыягід па Беларусі</span>
                </div>
            </a>
            <a href="keys-biblionoch.php" class="keys-blocks__item item item-bnoch">
                <div class="item-color item-color-pink"></div>
                <div class="item-bg-left"></div>
                <div class="item-title item-title-slavgorod">
                    Библионочь 2016
                </div>
            </a>

          
        </div>
      
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>   
