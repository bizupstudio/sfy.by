<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку новостей</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/news/4.svg" alt="img6">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__news-date">Автор: Виктория Щуровская</div>
                    <div class="blogs-rep__news-title">Не лезьте в чёрный ящик</div>
                    <div class="blogs-rep__news-disc">Из Википедии: "система, которую представляют как «чёрный ящик», рассматривается как имеющая некий «вход» для ввода информации и «выход» для отображения результатов работы, при этом происходящие в ходе работы системы процессы наблюдателю неизвестны."</div>
                </div>
                <div class="news-content">
                    <div class="box-content">
                        <p>
                            Также и стартап - какие-то данные на входе, потом внутри происходит что-то никому непонятное, и на выходе необъяснимый успех или закономерный провал. Посторонним можно сделать полезные выводы на основании очередной стартап-истории, но что делать тем, кто внутри чёрного ящика или на подходе к нему?
                        </p>
                        <h2>
                            SFY уже прошел свою "<a href="/news-dead.php">долину смерти</a>", кардинально поменявшись несколько раз на протяжении всего пути и поняв, что в изменениях нет ничего страшного.
                        </h2>
                        <p class="mrb80">
                            Теперь мы хотим поддержать таких же начинающих, как и мы когда-то. 
                        </p>
                        <h2>
                            Программа <span>Black Box</span> поможет в краткие сроки, но в спокойной обстановке понять, как можно развивать Вашу идею, подскажет пути ее реализации (независимо от тематики), выявит основные препятствия и способы их преодоления и подготовит проект к дальнейшей презентации инвесторам.

                        </h2>
                        <div class="title">BlackBox для стартапов</div>
                        <h2 class="mw">
                            В самом начале жизни стартапа, когда есть только идея и первые черновики, голова идет кругом от вопросов:
                        </h2>
                        <ul>
                            <li>как мечту реализовать в условиях реальной жизни?</li>
                            <li>за что хвататься в первую очередь - делать продукт, сервис, продвижение или деньги искать?</li>
                            <li>ходить по стартап-конференциям или тихо в подвале пилить MVP?</li>
                            <li>предлагать товар соседям или развиваться в онлайне?</li>
                            <li>одиноко надрываться самому или искать команду?</li>
                            <li>бросить всё ради проекта или совмещать с основной работой?</li>
                            <li>а может вообще не стоит начинать?</li>
                        </ul>
                    </div>
                    <div class="box-down">
                        <div class="col col-first">
                            <h2>Именно на эти вопросы и отвечает программа сопровождения проектов - <span>Black Box.</span></h2>
                            <p>Вам только нужно заполнить анкету, отправить ее на<br> <a href="mailto:sfyprojects@gmail.com" target="_blank">sfyprojects@gmail.com</a> и прийти на консультацию.
                            </p>
                        </div>
                        <div class="col col-link">
                            <a href="BlackBoxAnketa.doc" download>
                                <div class="img"></div>
                            </a>
                        </div>
                    </div>
                    <div class="box-after">
                        <div class="row">
                            <div class="col">
                                <div class="img img-1"></div>
                            </div>
                            <div class="col">
                                <div class="title">SWOT-анализ проектов</div>
                                <div class="disc">Если вы хотите убедиться в долгосрочных перспективах вашего стартапа, то должны периодически делать шаг назад и смотреть на вещи в более широком контексте. Так называемый SWOT-анализ создан специально для этого. С его помощью вы сможете не только оценить текущую эффективность вашей компании, но и понять, как она будет работать в будущем: через неделю, месяц или даже год.</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="img img-2"></div>
                            </div>
                            <div class="col">
                                <div class="title">Менторская поддержка</div>
                                <div class="disc">Иметь ментора – это большое преимущество. Ментор способен направить ваш поток мыслей, повысить эффективность обучения и сократить путь до цели. Он может показать невидимые до сих пор возможности и дать совет по их реализации. Словом, если вы берётесь за изучение квантовой механики, лучше это делать с учителем. То же касается и бизнеса, и актёрства, и писательства, и чего угодно</div>
                            </div>
                        </div>
                        <div class="end">
                            Приходите, и мы покажем Вам, что мечты исполняются и действительно возможно всё!
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>  
