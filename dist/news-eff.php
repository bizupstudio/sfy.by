<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SFY project group</title>
  <link href="https://fonts.googleapis.com/css?family=Cuprum%7CRoboto&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <link rel="stylesheet" href="css/style.css">
</head>
<body> 
  

<div class="wrapper news">
    <div class="wrapper-btn wrapper-btn-prev">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="company__icons">
        <a href="https://vk.com/sfy_group" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.facebook.com/sfygroup/" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://t.me/sfy_project" target="_blank"><div class="company__icons-icon"></div></a>
        <a href="https://www.instagram.com/sfy_project/?hl=ru" target="_blank"><div class="company__icons-icon"></div></a>
    </div>

    <div class="arrow"></div>
    <div class="arrow-disc"><a>Вернуться к списку новостей</a></div>
    <div class="news-container">
        
        <div class="blogs blogs-rep">
            <div class="blogs-title blogs-title-blog">Блог</div>
            <div class="blogs-rep__row">
                <div class="blogs-rep__img">
                    <div class="blogs-rep__image">
                        <img src="img/news/2.svg" alt="img6">
                    </div>
                </div>
                <div class="blogs-rep__caption">
                    <div class="blogs-rep__news-date">01.02.2019</div>
                    <div class="blogs-rep__news-title">Эффективное управление: рукоприкладство или психология?</div>
                   
                </div>
                <div class="news-content">
                    <div class="row-eff__header">
                        <div class="row">
                            <div class="col col-first">
                                <p>Руководить даже самой маленькой командой - задачка на любителя. Потому что нужно подумать вместо всех и обо всех.</p>
                                <h2>Поэтому чаще всего как руководитель вы сталкиваетесь:</h2>
                                <ul>
                                    <li>со множеством ошибок ввиду большой загруженности;</li>
                                    <li>неумением членов команды распределять свое время и внимание;</li>
                                    <li>конфликтами и недопониманием в группе.</li>
                                </ul>
                            </div>
                            <div class="col col-two">
                                <p>
                                    Первые два пункта хорошо гуглятся. Потому что там про механику управления персоналом. А вот третий - настоящая подстава. Потому что там про характеры.
                                </p>
                                <p>
                                    Техника безопасности: любая личность состоит из огромного количества проявлений. Поэтому, господа руководители, не торопимся вешать ярлыки на своих сотрудников.
                                </p>
                                <h2>
                                    Помним: главное - понять и направить в продуктивное русло, остальное - лирика.

                                </h2>
                            </div>
                        </div>
                        <div class="title">Мы рассмотрим три базовых типа личности, чтобы хоть как-то разобраться в “этих ваших психологиях”</div>
                        <div class="row-baz">
                            <div class="col">
                                <div class="img img-pink"></div>
                                <div class="title title-pink">Эпилептоидный</div>
                                <p>педантичный, скрупулезный, часто угрюмый тип. Способен долго накапливать эмоции, чтобы потом их выплеснуть на мощно заряженной волне.</p>
                                <p><span class="pink">Плюсы: </span> способен долго удерживать внимание на однообразной работе. Точен и аккуратен. Задания, как правило, выполняет в срок.</p>
                                <p><span class="pink">Поощрение: </span>так как эти ребята всегда опираются на факты, то они по достоинству оценят обещанную сумму в качестве бонуса.</p>
                                <p><span class="pink">Обходные пути: </span> дайте эпилептоиду вовремя слить эмоциональное напряжение. Его видимое спокойствие - часто затишье перед бурей. Он же накапливает злость. Периодически вызывайте его на откровенный разговор. Он поделится переживаниями, если захочет. Посоветуйте ему завести себе такое увлечение, которое предполагает приличный выброс адреналина. На батуты сходить, тайским боксом заняться. Или картинг, на худой конец.</p>
                            </div>
                            <div class="col">
                                <div class="img img-green"></div>
                                <div class="title title-green">Шизоидный</div>
                                <p>замкнутый, чудаковатый интроверт, с богатым внутренним миром и высоким уровнем интеллекта. В эмоциях либо сверхчувствителен, либо до одурения туг.</p>
                                <p><span class="green">Плюсы: </span> шизоид создаст костяк, систему проекта и просмотрит все варианты его успешной работы “на глубине”. Методы в работе обычно использует неоднозначные, но эффективные.</p>
                                <p><span class="green">Поощрение: </span> не мешать. Давать относительную свободу в принятии решений. Ценить интеллектуальность, но не трепаться об этом. И, конечно, денежка.</p>
                                <p><span class="green">Обходные пути: </span>шизоид может “закопаться” и начать тормозить. Просто потому что всю ночь ему было очень интересно смотреть видео про жуков-голиафов. Так что периодически заглядывайте в его монитор со словами: “Вот это жучище! А забеги сегодня ко мне с отчетом, к часикам четырем”. А еще некоторые, для вас очевидные, вещи ему придется объяснять на пальцах. Потому что он на самом деле не сечет.</p>
                            </div>
                            <div class="col">
                                <div class="img img-fiol"></div>
                                <div class="title title-fiol">Истероидный</div>
                                <p>любитель привлекать к себе внимание, яркий, шустрый, обаятельный. Его настроение очень изменчиво: постоянно колеблется от дикого раздражения до вселенской радости. И наоборот.</p>
                                <p><span class="fiol">Плюсы: </span> истероид лучше других видит картинку происходящего. Во-первых, он знает, как привлечь внимание покупателя. А во-вторых, может поговорить с клиентом так, чтобы среди всех прочих выбрали его, а соответственно, и вас.</p>
                                <p><span class="fiol">Поощрение: </span>хвалить много, но по заслугам. Оценивать его личный вклад в развитие проекта. Специально для этих ребят придуманы благодарности и бонусы неденежного образца. Но и на кэш не скупитесь, пожалуйста.</p>
                                <p><span class="fiol">Обходные пути: </span>дайте истероиду возможность делать небольшие частые перерывы в работе. А говорить с ними лучше, как с детьми: “Ты все делаешь правильно, ты молодец”. Так он сам будет докладывать вам об успехах проекта. Правда, о неудачах будет стремиться умолчать.</p>
                            </div>
                        </div>
                        <div class="row-af">
                            <div class="col">
                                <p>Так, опираясь на типы личности членов вашей дримтим, вы сможете посадить каждого на свое место и дать в руки свою эффективную игрушку.
                                   </p>
                                   <h2>И дело однозначно пойдет успешнее.</h2>
                            </div>
                            <div class="col">
                                <p>А вот как облегчить себе жизнь, когда вы - часть команды и как вычислить тип-диверсант среди коллег и клиентов, <a href="#">читайте в следующих статьях</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery.fullpage.js"></script> 
  <script src="js/main.js"></script>
  <script src="js/owl.carousel.min.js"></script>
</body>
</html>  
